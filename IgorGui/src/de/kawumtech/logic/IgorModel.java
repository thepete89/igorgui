package de.kawumtech.logic;

import java.io.File;
import java.io.PipedOutputStream;
import java.util.Observable;

import de.kawumtech.task.FmodTaskHandler;
import de.kawumtech.task.IgorTaskHandler;
import de.kawumtech.task.TaskHandler;

/**
 * model containing the task data and results for igor gui and managing the execution of tasks
 * @author pete
 *
 */
public class IgorModel extends Observable {
	
	private String lastMessage;
	private TaskHandler lastTask;
	
	public IgorModel(){
		this.lastMessage = "";
		this.lastTask = null;
	}
	
	public void loadExampleFile(File file){
		// create loader task
		TaskHandler loadTask = new FmodTaskHandler(this, file);
		this.lastTask = loadTask;
		Thread taskThread = new Thread(loadTask);
		taskThread.setName("ExampleLoaderTask");
		// start loader task
		taskThread.start();
		
	}
	
	public void runIgor(String fmodFile, String fmodModule, String fmodExample, String extraKnowledge, String maudePath, String igorPath, PipedOutputStream console, boolean trace){
		TaskHandler igorTask = new IgorTaskHandler(fmodFile, fmodModule, fmodExample, extraKnowledge, maudePath, igorPath, console, trace);
		this.lastTask = igorTask;
		Thread taskThread = new Thread(igorTask);
		taskThread.setName("IgorTask");
		taskThread.start();
	}
	
	public boolean isWorking(){
		if(lastTask != null){
			return lastTask.isTaskActive();
		}
		return false;
	}
	
	public synchronized void setLastMessage(String message){
		this.lastMessage = message;
		triggerNotification();
	}
	
	public String getExecMessages(){
		String message = this.lastMessage;
		this.lastMessage = null;
		return message;
	}
	
	public TaskHandler getLastTask(){
		// improved memory management - temporarily save last task, then do a null reference
		// should destroy the last taskhandler
		TaskHandler last = this.lastTask;
		this.lastTask = null;
		return last;
	}
	
	public synchronized void stopCurrentTask(){
		this.lastTask.cancelTask();
	}
	
	public synchronized void signalizeUpdate(){
		triggerNotification();
	}
	
	private void triggerNotification(){
		setChanged();
		notifyObservers();
	}
	
}
