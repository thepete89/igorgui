package de.kawumtech.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper for Javas {@link ProcessBuilder} used to handle maude
 * @author pete
 *
 */
public class ExtManager {

	private Process prog;
	private String command;
	private String params;
	private InputStream progOut;
	private OutputStream progIn;

	public ExtManager(String command, String params) {
		this.command = command;
		this.params = params;
	}

	public void execute() throws RuntimeException {
		final String os = osHelper();
		
		// executes given command with os-specific call syntax
		if ("WINDOWS".equals(os)) {
			try {
				executeWin();
			} catch (IOException e) {
				throw new RuntimeException("ERROR: Could not execute command "
						+ command + " under operating system " + os);
			}
		} else if ("OTHER".equals(os)) {
			try {
				executeOther();
			} catch (IOException e) {
				throw new RuntimeException("ERROR: Could not execute command "
						+ command + " under operating system " + os);
			}
		} else {
			throw new RuntimeException(
					"ERROR: Could not determine operating system - command execution impossible!");
		}
	}

	// helper methods
	private String osHelper() {
		// check which operating system is used
		String data = System.getProperty("os.name");
		String os;

		if (data != null) {
			if (data.toLowerCase().indexOf("windows") != -1) {
				os = "WINDOWS";
			} else {
				os = "OTHER";
			}
		} else {
			os = "UNKNOWN";
		}
		return os;
	}

	private void executeWin() throws IOException {
		// executes an external program, windows style
		String[] args = params.split(" ");
		List<String> execCommand = new ArrayList<>();
		execCommand.add(command);
		for (int i = 0; i < args.length; i++) {
			execCommand.add(args[i]);
		}
		ProcessBuilder pb = new ProcessBuilder(execCommand);
		prog = pb.start();
		progOut = prog.getInputStream();
		progIn = prog.getOutputStream();
		
	}

	private void executeOther() throws IOException {
		// execute an external program, linux/unix style
		ProcessBuilder pb = new ProcessBuilder(command, params);
		prog = pb.start();
		progOut = prog.getInputStream();
		progIn = prog.getOutputStream();
		
	}

	/**
	 * retrieve process output
	 * 
	 * @return the InputStream connected to the running process
	 */
	public InputStream getProcessOutput() {
		return progOut;
	}

	/**
	 * retrieve process input
	 * 
	 * @return the OutputStream connected to the running process
	 */
	public OutputStream getProcessInput() {
		return progIn;
	}
		
	/**
	 * closes the called program handler
	 */
	public void close(){
		prog.destroy();
	}
	
	/**
	 * waits for the sub-process to finish and returns its exit code
	 * @return the exit code of the called program - should be zero or there was an error
	 * @throws InterruptedException
	 */
	public int waitFor() throws InterruptedException{
		// wait for processes to finish
		int status = prog.waitFor();
		return status;
	}
	
	/**
	 * returns the exit value of the subprocess
	 * @return the exit code of the called program - should be zero or there was an error
	 */
	public int status(){
		return prog.exitValue();
	}
}
