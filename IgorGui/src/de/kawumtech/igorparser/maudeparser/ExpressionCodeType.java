package de.kawumtech.igorparser.maudeparser;

/**
 * Enum distinguishing the different {@link ExpressionCodeNode} types
 * @author pete
 *
 */
public enum ExpressionCodeType {
	EXPRESSION, ARGUMENTS, VARIABLE, CONSTANT;
}
