# README #

This is the main repository for IgorGui, developed during the CogSys Bachelor Project in Summer 2014 at the University of Bamberg.

### General Program Information ###

* This program is used to give a convenient user interface for Maude and the IGOR-System used in the project
* Currently version 0.9 - it would need a few improvements, but we had no more time for that
* The code in this repository is free and open source software licensed under GNU General Public License v3
* External libraries (namely: parboiled for Java, MigLayout) used in this project belong to their respective owners and have licenses which can differ from the license stated above 

### How do I get set up? ###
This repository contains: The main source code for IgorGUI, all dependencies used during creation and files to import everything as an eclipse project

### Who do I talk to? ###
If you have any questions contact the creator of this program, Peter Hohmann, via e-mail: pete@kawumtech.de