package de.kawumtech.igorparser;

import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.SuppressNode;
import org.parboiled.annotations.SuppressSubnodes;
import org.parboiled.support.StringVar;

import de.kawumtech.igorparser.maudeparser.EquationCodeRoot;
import de.kawumtech.igorparser.maudeparser.ExpressionCodeNode;
import de.kawumtech.igorparser.maudeparser.ExpressionCodeType;

/**
 * This class implements a simple parser that transforms IGORs equation output into a tree representation,
 * using a subset of the full grammar used in the main parser. At the moment the tree is only generated for
 * normal equations. Conditional equations do NOT create a tree root!
 * @author pete
 *
 */
@BuildParseTree
public class MaudeCodeParser extends BaseParser<Object> {
	//
	// Basic Tokens
	//
	@SuppressNode
	Rule LSBR() {
		return Ch('[');
	}

	@SuppressNode
	Rule RSBR() {
		return Ch(']');
	}

	@SuppressNode
	Rule LPAR() {
		return Ch('(');
	}

	@SuppressNode
	Rule RPAR() {
		return Ch(')');
	}

	@SuppressNode
	Rule QT() {
		return Ch('\'');
	}

	@SuppressNode
	Rule EQ() {
		return Ch('=');
	}

	@SuppressNode
	Rule COLON() {
		return Ch(':');
	}

	@SuppressNode
	Rule COMMA() {
		return Ch(',');
	}

	@SuppressNode
	Rule DOT() {
		return Ch('.');
	}

	@SuppressNode
	Rule CONDEQ() {
		return String("_==_");
	}

	@SuppressNode
	Rule EQRULE() {
		return String("eq");
	}

	@SuppressNode
	Rule CEQRULE() {
		return String("ceq");
	}

	@SuppressNode
	Rule WEDGE() {
		return String("/\\");
	}

	@SuppressNode
	Rule IF() {
		return String("if");
	}

	@SuppressNode
	Rule TRUE() {
		return String("true");
	}

	@SuppressNode
	Rule FALSE() {
		return String("false");
	}

	@SuppressNode
	Rule BOOLTY() {
		return String("Bool");
	}
	
	// combined symbol expressions (strings, ids..)
	@SuppressNode
	Rule SYMBOL() {
		return FirstOf('[', ']', ',');
	}


	@SuppressNode
	Rule CHAR() {
		return FirstOf(OneOrMore(CharRange('a', 'z')),
				OneOrMore(CharRange('A', 'Z')), '<', '>', '_', '=', '#','"', ' ');
	}

	@SuppressSubnodes
	Rule STRING() {
		return OneOrMore(CHAR());
	}

	Rule DIGIT() {
		return CharRange('0', '9');
	}

	@SuppressSubnodes
	Rule NUMBER() {
		return OneOrMore(DIGIT());
	}

	@SuppressNode
	Rule NEWLINE() {
		return Sequence(Optional("\r"), "\n");
	}

	@SuppressSubnodes
	Rule ID() {
		return OneOrMore(FirstOf(STRING(), NUMBER()));
	}
	
	@SuppressSubnodes
	Rule OP(){
		return Sequence(SYMBOL(), ZeroOrMore(SYMBOL()));
	}
	
	@SuppressNode
	Rule WHITESPACE() {
		return ZeroOrMore(AnyOf(" \t\u000C"));
	}
	
	// Base Parser Rules
	// Simple Rules/Terminating Rules
	Rule bool() {
		return Sequence(QT(), FirstOf(TRUE(), FALSE()), DOT(), BOOLTY());
	}

	Rule vartype() {
		StringVar type = new StringVar();
		return Sequence(COLON(), ID(), type.set(matchOrDefault("nil")), push(new ExpressionCodeNode(type.get(), ExpressionCodeType.VARIABLE)));
	}

	Rule consttype() {
		StringVar type = new StringVar();
		return Sequence(DOT(), ID(), type.set(matchOrDefault("nil")), push(new ExpressionCodeNode(type.get(), ExpressionCodeType.CONSTANT)));
	}
	
	// equation rule
	Rule equation(){
		return FirstOf(
				Sequence(EQRULE(), expression(), EQ(), expression(), push(new EquationCodeRoot((ExpressionCodeNode) pop(1),(ExpressionCodeNode) pop()))), 
				Sequence(CEQRULE(), expression(), EQ(), expression(), ifexpression())
			);
	}

	// expression
	Rule expression() {
		StringVar id = new StringVar();
		return Sequence(WHITESPACE(), FirstOf(ID(), OP()), id.set(matchOrDefault("nil")),
				FirstOf(funbody(), vartype(), consttype()), WHITESPACE(), push(new ExpressionCodeNode(id.get(), ExpressionCodeType.EXPRESSION, (ExpressionCodeNode) pop())));
	}

	// ifexpression
	Rule ifexpression() {
		return Sequence(IF(), cond(), ZeroOrMore(Sequence(WEDGE(), cond())));
	}

	// args
	Rule args() {
		return Sequence(
				expression(),
				ZeroOrMore(Sequence(COMMA(), Optional(WHITESPACE()), expression(), push(new ExpressionCodeNode("ARGS", ExpressionCodeType.ARGUMENTS, (ExpressionCodeNode) pop(1), (ExpressionCodeNode) pop()))))
				);
	}

	// funbody
	Rule funbody() {
		return Sequence(LSBR(), args(), RSBR());
	}

	// condition
	Rule cond() {
		return Sequence(CONDEQ(), LSBR(), expression(), COMMA(),
				expression(), RSBR(), EQ(), bool());
	}
		
}
