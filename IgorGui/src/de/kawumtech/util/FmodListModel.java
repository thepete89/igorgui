package de.kawumtech.util;

import java.util.Vector;

import javax.swing.AbstractListModel;

/**
 * Custom list model for the example list
 * @author pete
 *
 */
public class FmodListModel extends AbstractListModel<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * List data
	 */
	private Vector<FmodDataModel> listData;
	
	public FmodListModel(){
		this.listData = new Vector<FmodDataModel>();
	}
	
	public FmodListModel(Vector<FmodDataModel> listData) {
		super();
		this.listData = listData;
	}

	@Override
	public String getElementAt(int pos) {
		return listData.elementAt(pos).getModuleName();
	}

	@Override
	public int getSize() {
		return listData.size();
	}
	
	public void addListElement(FmodDataModel moduleData){
		listData.add(moduleData);
		fireIntervalAdded(this, 0, getSize());
	}
	
	public void removeListElementAt(int pos){
		listData.remove(pos);
		fireIntervalRemoved(this, 0, getSize());
	}
	
	public void clearData(){
		listData.clear();
		fireContentsChanged(this, 0, getSize());
	}
	
	public FmodDataModel getModuleDataAt(int pos){
		return listData.elementAt(pos);
	}
}
