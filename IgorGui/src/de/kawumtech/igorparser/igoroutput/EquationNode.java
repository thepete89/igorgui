package de.kawumtech.igorparser.igoroutput;

/**
 * Wrapperclass representing all equations for a specific hypothese
 * @author pete
 *
 */
public class EquationNode {
	private String leftPart = "";
	private String rightPart = "";
	private String ifPart = "";
	private boolean isIf = false;
	
	public final String getLeft(){
		return leftPart;
	}
	
	public final String getRight(){
		return rightPart;
	}
	
	public final String getIf(){
		return ifPart;
	}
	
	public final boolean isIf(){
		return isIf;
	}
	
	public final EquationNode insertData(String left, String right){
		this.leftPart = left.trim();
		this.rightPart = right.trim();
		return this;
	}
	
	public final EquationNode insertData(String left, String right, String ifPart){
		this.leftPart = left.trim();
		this.rightPart = right.trim();
		this.ifPart = ifPart.trim();
		this.isIf = true;
		return this;
	}
}
