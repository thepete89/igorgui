package de.kawumtech.igorparser;

import org.parboiled.BaseParser;
import org.parboiled.Rule;
import org.parboiled.annotations.BuildParseTree;
import org.parboiled.annotations.SuppressNode;
import org.parboiled.annotations.SuppressSubnodes;
import org.parboiled.support.Var;

import de.kawumtech.igorparser.igoroutput.EquationNode;
import de.kawumtech.igorparser.igoroutput.HypoList;
import de.kawumtech.igorparser.igoroutput.HypotheseNode;

/**
 * Output parser for IGOR output, realized with parboiled
 * 
 * @author pete
 * 
 */
@BuildParseTree
public class IgorOutputParser extends BaseParser<Object> {
	//
	// Basic Tokens
	//
	@SuppressNode
	Rule LSBR() {
		return Ch('[');
	}

	@SuppressNode
	Rule RSBR() {
		return Ch(']');
	}

	@SuppressNode
	Rule LPAR() {
		return Ch('(');
	}

	@SuppressNode
	Rule RPAR() {
		return Ch(')');
	}

	@SuppressNode
	Rule QT() {
		return Ch('\'');
	}

	@SuppressNode
	Rule EQ() {
		return Ch('=');
	}

	@SuppressNode
	Rule ESC() {
		return Ch('`');
	}

	@SuppressNode
	Rule COLON() {
		return Ch(':');
	}

	@SuppressNode
	Rule COMMA() {
		return Ch(',');
	}

	@SuppressNode
	Rule DOT() {
		return Ch('.');
	}

	@SuppressNode
	Rule CONDEQ() {
		return String("_==_");
	}

	@SuppressNode
	Rule EQRULE() {
		return String("eq");
	}

	@SuppressNode
	Rule CEQRULE() {
		return String("ceq");
	}

	@SuppressNode
	Rule WEDGE() {
		return String("/\\");
	}

	@SuppressNode
	Rule IF() {
		return String("if");
	}

	@SuppressNode
	Rule HYPO() {
		return String("hypo");
	}

	@SuppressNode
	Rule TRUE() {
		return String("true");
	}

	@SuppressNode
	Rule FALSE() {
		return String("false");
	}

	@SuppressNode
	Rule BOOLTY() {
		return String("Bool");
	}

	@SuppressNode
	Rule HYPOTHESES() {
		return String("hypotheses");
	}

	@SuppressNode
	Rule HYPOTHESE() {
		return String("hypothese");
	}

	// combined symbol expressions (strings, ids..)
	@SuppressNode
	Rule SYMBOL() {
		return FirstOf('[', ']', ',');
	}

	@SuppressNode
	Rule ESCCHAR() {
		return Sequence(ESC(), SYMBOL());
	}

	@SuppressNode
	Rule CHAR() {
		return FirstOf(OneOrMore(CharRange('a', 'z')),
				OneOrMore(CharRange('A', 'Z')), '<', '>', '_', '=', '#','"', ' ');
	}

	@SuppressSubnodes
	Rule STRING() {
		return OneOrMore(FirstOf(CHAR(), ESCCHAR()));
	}

	Rule DIGIT() {
		return CharRange('0', '9');
	}

	@SuppressSubnodes
	Rule NUMBER() {
		return OneOrMore(DIGIT());
	}

	@SuppressNode
	Rule NEWLINE() {
		return Sequence(Optional("\r"), "\n");
	}

	@SuppressNode
	Rule LEND() {
		return FirstOf("[ none] .", "[none] .");
	}

	@SuppressSubnodes
	Rule ID() {
		return OneOrMore(FirstOf(STRING(), NUMBER()));
	}

	@SuppressNode
	Rule WHITESPACE() {
		return ZeroOrMore(AnyOf(" \t\u000C"));
	}

	// Base Parser Rules
	// Simple Rules/Terminating Rules
	@SuppressSubnodes
	Rule bool() {
		return Sequence(QT(), FirstOf(TRUE(), FALSE()), DOT(), BOOLTY());
	}

	@SuppressSubnodes
	Rule vartype() {
		return Sequence(COLON(), ID());
	}

	@SuppressSubnodes
	Rule consttype() {
		return Sequence(DOT(), ID());
	}

	// hypotheses rule
	Rule hypotheses() {
		Var<HypoList> hypos = new Var<>(new HypoList());
		return Sequence(hypothese(hypos),
				ZeroOrMore(Sequence(Optional(WHITESPACE()), hypothese(hypos))),
				EOI, push(hypos.getAndClear()));
	}

	// hypothese rule
	Rule hypothese(final Var<HypoList> hypos) {
		Var<HypotheseNode> hypo = new Var<>(new HypotheseNode());
		return Sequence(HYPO(), LPAR(), FirstOf(TRUE(), FALSE()), COMMA(),
				WHITESPACE(), ID(), COMMA(), WHITESPACE(),
				OneOrMore(equation(hypo)), RPAR(),
				push(hypos.get().add(hypo.get())));
	}

	// equation rule
	Rule equation(final Var<HypotheseNode> hypo){
		Var<String> left = new Var<>();
		Var<String> right = new Var<>();
		Var<String> ifexp = new Var<>();
		return FirstOf(
				Sequence(EQRULE(), expression(), left.set(matchOrDefault("")), EQ(), expression(), right.set(matchOrDefault("")), LEND(), push(hypo.get().add(new EquationNode().insertData(left.get(), right.get())))), 
				Sequence(CEQRULE(), expression(), left.set(matchOrDefault("")), EQ(), expression(), right.set(matchOrDefault("")), ifexpression(), ifexp.set(matchOrDefault("")), LEND(), push(hypo.get().add(new EquationNode().insertData(left.get(), right.get(), ifexp.get()))))
			);
	}

	// expression
	@SuppressSubnodes
	Rule expression() {
		return Sequence(WHITESPACE(), QT(), ID(),
				FirstOf(funbody(), vartype(), consttype()), WHITESPACE());
	}

	// ifexpression
	@SuppressSubnodes
	Rule ifexpression() {
		return Sequence(IF(), cond(), ZeroOrMore(Sequence(WEDGE(), cond())));
	}

	// args
	Rule args() {
		return Sequence(
				expression(),
				ZeroOrMore(Sequence(COMMA(), Optional(WHITESPACE()),
						expression())));
	}

	// funbody
	@SuppressSubnodes
	Rule funbody() {
		return Sequence(LSBR(), args(), RSBR());
	}

	// condition
	@SuppressSubnodes
	Rule cond() {
		return Sequence(QT(), CONDEQ(), LSBR(), expression(), COMMA(),
				expression(), RSBR(), EQ(), bool());
	}

}
