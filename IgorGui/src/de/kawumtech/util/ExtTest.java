package de.kawumtech.util;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Scanner;

/*
 * Test class for ExtManager
 */
public class ExtTest {

	public static void main(String[] args) {
		System.out.println("TESTING ExtManager");
		// call a windows command
		ExtManager mgrWin = new ExtManager("cmd", "/c dir");
		try {
			mgrWin.execute();
			System.out.println("EXTERNAL PROGRAMM CALLED - SHOWING OUTPUT:");
			Scanner out = new Scanner(mgrWin.getProcessOutput());
			while(out.hasNext()){
				System.out.println("> " + out.nextLine());
			}
			out.close();
			mgrWin.close();
		} catch (Exception e) {
			System.err.println("Caught exception: " + e.getMessage());
		}

		// call a linux/unix command
		ExtManager mgr = new ExtManager("maude", "");
		try {
			mgr.execute();
			PrintWriter in = new PrintWriter(new BufferedWriter(new OutputStreamWriter(mgr.getProcessInput())));
			Scanner out = new Scanner(mgr.getProcessOutput());
			in.println("load /home/pete/Downloads/igor/igor2.2.maude");
			in.flush();
			in.println("load /home/pete/Downloads/igor/ecaiproblems.maude");
			in.flush();
			in.println("red in IGOR : gen('LAST, 'last, noName) .");
			in.flush();
			in.println("eof");
			in.flush();
			System.out.println("EXTERNAL PROGRAMM CALLED - SHOWING OUTPUT:");
			while(out.hasNext()){
				System.out.println("> " + out.nextLine());
			}
			in.close();
			out.close();
			mgr.close();
		} catch (Exception e) {
			System.err.println("Caught exception: " + e.getMessage());
		}
		
		System.out.println("DONE");
	}

}
