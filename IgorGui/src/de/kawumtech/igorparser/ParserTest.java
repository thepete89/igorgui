package de.kawumtech.igorparser;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.parboiled.Parboiled;
import org.parboiled.errors.ErrorUtils;
import org.parboiled.parserunners.ReportingParseRunner;
import org.parboiled.support.ParsingResult;

import de.kawumtech.igorparser.igoroutput.EquationNode;
import de.kawumtech.igorparser.igoroutput.HypoList;
import de.kawumtech.igorparser.igoroutput.HypotheseNode;
import de.kawumtech.igorparser.maudeparser.EquationCodeRoot;


/*
 * WARNING: This is 100% pure test code used to develop the parsers - chaos ahead!
 * No refactoring nor any code cleanup done!
 */
public class ParserTest {

	public static void main(String[] args) throws Exception {
		String testfile = "lib/output1.txt";
		List<String> lines = Files.readAllLines(Paths.get(testfile), Charset.defaultCharset());
		StringBuilder data = new StringBuilder();
		for (String string : lines) {
			data.append(string);
		}
		
		String output = data.toString();
		
		IgorOutputParser parser = Parboiled.createParser(IgorOutputParser.class);
		ReportingParseRunner<?> runner = new ReportingParseRunner<>(parser.hypotheses());
		ParsingResult<?> result = runner.run(output);
		HypoList hypos = (HypoList) result.resultValue;
		System.out.println("--------------- IGOR OUTPUT ---------------");
		System.out.println("HYPOTHESES FOUND: " + hypos.getHypolistSize());
		int i = 1;
		List<List<String>> hypoCode = new ArrayList<>();
		for (HypotheseNode node : hypos.getHypos()) {
			System.out.println("HYPOTHESE  #" + i);
			System.out.println("- EQUATIONS: " + node.getEquationCount());
			
			List<EquationNode> equations = node.getEquations();
			List<String> eqRaw = new ArrayList<>();
			for (EquationNode equationNode : equations) {
				if(equationNode.isIf()){
					System.out.println("\tEQUATION: IF " + transformString(equationNode.getLeft()) + " EQUALS " + transformString(equationNode.getRight()) + " THEN " + transformString(equationNode.getIf()));
				} else {
					System.out.println("\tEQUATION: " + transformString(equationNode.getLeft()) + " = " + transformString(equationNode.getRight()));
					eqRaw.add("eq "+ transformString(equationNode.getLeft()) + " = " + transformString(equationNode.getRight()));
				}		
			}
			hypoCode.add(eqRaw);
			i++;
		}
		
		if(result.hasErrors()) {
			System.out.println("--------------- PARSER ERRORS -------------");
			ErrorUtils.printParseErrors(result.parseErrors);
		}
		
		System.out.println("-------------- GENERATED CODE -------------");
		
		for (List<String> equations : hypoCode) {
			List<EquationCodeRoot> equationRoots = new ArrayList<>();
			for (String equation : equations) {
				MaudeCodeParser cp = Parboiled.createParser(MaudeCodeParser.class);
				ReportingParseRunner<?> cpr = new ReportingParseRunner<>(cp.equation());
				ParsingResult<?> res = cpr.run(equation);
				EquationCodeRoot parserResult = (EquationCodeRoot) res.resultValue;
				equationRoots.add(parserResult);
								
			}
			
			MaudeCodeGenerator codegen = new MaudeCodeGenerator(equationRoots);
			codegen.startCodeGen();
			
			Map<String, List<String>> vars = codegen.getVars();
			StringBuilder varCode = new StringBuilder();
			for (java.util.Map.Entry<String, List<String>> entry : vars.entrySet()) {
				String type = entry.getKey();
				List<String> varlist = entry.getValue();
				if(varlist.size() > 1){
					varCode.append("vars ");
				} else {
					varCode.append("var ");
				}
				for (String var : varlist) {
					varCode.append(var + " ");
				}
				varCode.append(": " + type + " .\n");
			}
			System.out.println(varCode.toString());
			System.out.println(codegen.getCode());
		}
	}
	
	private static String transformString(String input){
		return input.replaceAll("`", "").replaceAll("'", "");
	}
		
}
