package de.kawumtech.igorparser.igoroutput;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapperclass for hypotheses parser - contains all found {@link HypotheseNode}s
 * @author pete
 *
 */
public class HypoList {
	private final List<HypotheseNode> hypos = new ArrayList<>();
	
	public HypoList add(final HypotheseNode hypothese){
		hypos.add(hypothese);
		return this;
	}
	
	public List<HypotheseNode> getHypos(){
		return hypos;
	}
	
	public int getHypolistSize(){
		return hypos.size();
	}
	
	public HypotheseNode getHypoAt(int index){
		return hypos.get(index);
	}

}
