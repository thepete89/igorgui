package de.kawumtech.task;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import de.kawumtech.logic.IgorModel;
import de.kawumtech.util.FmodDataModel;

/**
 * {@link TaskHandler} implemented for loading and processing maude-files
 * containing examples for igor to learn
 * @author pete
 *
 */
public class FmodTaskHandler implements TaskHandler {
	
	private boolean isActive = false;
	private IgorModel model;
	private ArrayList<FmodDataModel> fmodData;
	private File fmodFile;
	
	public FmodTaskHandler(IgorModel model, File fmodFile){
		this.model = model;
		this.fmodFile = fmodFile;
		this.fmodData = new ArrayList<>();
	}
	
	@Override
	public void run() {
		startTask();
	}

	@Override
	public void startTask() {
		this.isActive = true;
		ArrayList<String> fmods = parseFile();
		processFmods(fmods);
		
		this.isActive = false;
		pushTaskOutput("[EXAMPLE-LOADER] Done.");
		// this.model.signalizeUpdate();
	}

	private ArrayList<String> parseFile() {
		pushTaskOutput("[EXAMPLE-LOADER] PARSING FILE " + fmodFile.getAbsolutePath());
		ArrayList<String> fmods = new ArrayList<>();
		
		try {
			Scanner scanner = new Scanner(fmodFile);
			StringBuilder fmodTmp = new StringBuilder();
			boolean fmodFound = false;
			
			// "parse" file line by line - regex did not work so well...
			while(scanner.hasNextLine()) {
				String line = scanner.nextLine();
				
				if(line.startsWith("fmod")){
					fmodFound = true;
				}
				if(line.startsWith("endfm")) {
					fmodTmp.append(line + "\n");
					fmods.add(fmodTmp.toString());
					fmodTmp.setLength(0);
					fmodFound = false;
					// skip to next step in loop
					continue;
				} 
				if(fmodFound){
					fmodTmp.append(line + "\n");
				}
			}
			scanner.close();
			
			
		} catch (FileNotFoundException e) {
			pushTaskOutput("[EXAMPLE-LOADER] Error: Could not parse file " + fmodFile.getAbsolutePath());
			e.printStackTrace();
		}
		
		pushTaskOutput("[EXAMPLE-LOADER] File " + fmodFile.getAbsolutePath() + " read successfull! Found " + fmods.size() + " maude definitions");
		return fmods;
	}
	
	private void processFmods(ArrayList<String> fmods){
		pushTaskOutput("[EXAMPLE-LOADER] Processing parsed file data");
		
		int i = 1;
		for (String fmod : fmods) {
			pushTaskOutput("[EXAMPLE-LOADER] Processing fmod definition " + i + " of " + fmods.size());
			
			Scanner data = new Scanner(fmod);
			String name = "";
			String code = "";
			ArrayList<String> functionsTemp = new ArrayList<>();
			while(data.hasNextLine()){
				String line = data.nextLine();
				// remove tabs and leading/trailing whitespaces
				line = line.replaceAll("\\t", " ");
				line = line.trim();
				if(line.startsWith("fmod")){
					name = line.split(" ")[1];
					pushTaskOutput("[EXAMPLE-LOADER] Processing data for maude module " + name);
				}
				if(line.startsWith("eq")){
					// hope this works?
					String funcDef = line.split(" ")[1];
					String funcName = funcDef.substring(0, funcDef.indexOf('('));
					if(!functionsTemp.contains(funcName)){
						pushTaskOutput("[EXAMPLE-LOADER] Found example " + funcName + " in maude module " + name);
						functionsTemp.add(funcName);
					}
					
				}
				
				code += line + "\n";
			}
			data.close();
			String[] functions = functionsTemp.toArray(new String[functionsTemp.size()]);
			fmodData.add(new FmodDataModel(name, code, functions));
			
			i++;
		}
		
		pushTaskOutput("[EXAMPLE-LOADER] Processing fmod definitions finished.");
		
	}

	@Override
	public boolean isTaskActive() {
		return isActive;
	}

	@Override
	public Object getResults() {
		return fmodData;
	}

	@Override
	public void pushTaskOutput(String output) {
		this.model.setLastMessage(output);
	}

	@Override
	public TaskTypes getTaskType() {
		return TaskTypes.TASKFMOD;
	}

	@Override
	public void cancelTask() {
		// this task is quick, so no cancel here
		return;
	}

}
