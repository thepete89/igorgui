package de.kawumtech.task;

/**
 * The TaksHandler interface introduces a strategy pattern for the various
 * tasks, providing convenient access to their results and outputs and handles
 * threading and parallel processing
 * 
 * @author Pete
 * 
 */
public interface TaskHandler extends Runnable {

	/**
	 * startTask - contains the main task logic that is executed by the calling
	 * thread. This method can be called directly, but it is advised to wrap it
	 * in the implementations run method to make it threaded
	 */
	public abstract void startTask();

	/**
	 * isTaskActive - returns the state of the task
	 * 
	 * @return true if running, false otherwise
	 */
	public abstract boolean isTaskActive();

	/**
	 * getResults - returns the so far processed data of the running task or the
	 * final output when the task has finished
	 * 
	 * @return an Object containing the tasks results (could be an ArrayList,
	 *         Vector or something similar, depending on implementation)
	 */
	public abstract Object getResults();

	/**
	 * pushTaskOutput - pushes output (like console messages or error
	 * information) to the calling model thread for further handling
	 * 
	 * @param a
	 *            String containing the required information
	 */
	public abstract void pushTaskOutput(String output);

	/**
	 * getTaskType - returns the type of the task, defined by {@link TaskTypes}
	 * 
	 * @return a predefined element from {@link TaskTypes}
	 */
	public abstract TaskTypes getTaskType();

	/**
	 * cancelTask - function that can be implemented when a task should be
	 * stopable
	 */
	public abstract void cancelTask();

}
