package de.kawumtech.igorparser.igoroutput;

import java.util.ArrayList;
import java.util.List;

/**
 * Wrapperclass holding all equations for one hypothese
 * @author pete
 *
 */
public class HypotheseNode {
	private final List<EquationNode> equations = new ArrayList<>();
	
	public HypotheseNode add(final EquationNode equation){
		equations.add(equation);
		return this;
	}
	
	public List<EquationNode> getEquations(){
		return equations;
	}
	
	public int getEquationCount(){
		return equations.size();
	}
	
	public EquationNode getEquationAt(int index){
		return equations.get(index);
	}
}
