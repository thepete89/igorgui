package de.kawumtech.gui;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;

import net.miginfocom.swing.MigLayout;
import de.kawumtech.logic.IgorController;
import de.kawumtech.logic.IgorModel;
import de.kawumtech.task.TaskHandler;
import de.kawumtech.task.TaskTypes;
import de.kawumtech.util.FmodDataModel;
import de.kawumtech.util.FmodListModel;

import javax.swing.ScrollPaneConstants;
import javax.swing.JCheckBox;

/**
 * The main GUI class
 * 
 * @author pete
 * 
 */
public class IgorGuiMainwindow implements Observer {

	// ui elements
	private JFrame frmIgorgui;
	private JScrollPane scrollPaneConsole;
	private JTextPane consolePane;
	private JLabel lblConsole;
	private JLabel lblMaude;
	private JLabel lblIgor;
	private JTextField pathMaude;
	private JTextField pathIgor;
	private JButton loadMaude;
	private JButton loadIgor;
	private JList<String> fmodList;
	private JButton btnRun;
	private JScrollPane scrollPaneFmod;
	private JTextPane fmodPane;
	private JButton loadExamples;
	private JProgressBar progressBar;
	private JSeparator separator;
	private JCheckBox chckbxEnableTracing;
	private JButton btnKillIgor;
	private JLabel lblStatus;
	private JButton btnAddKnowledge;
	private JScrollPane scrollPaneFmodlist;

	// controller, model, fmodlist-data
	private IgorController controller;
	private IgorModel model;
	private FmodListModel listData;

	// last opened file - for convenience
	private File lastOpenedFolder;

	// holds the current igor worker
	private IgorGuiConsoleWorker guiWorker;
	// memory check for worker
	private Timer memChecker;

	// extra knowledge for igor - default noName
	private String extraKnowledge = "noName";

	/**
	 * Create the application window.
	 */
	public IgorGuiMainwindow(IgorController controller, IgorModel model) {
		this.controller = controller;
		this.model = model;
		initialize();
		addActionListeners();
		this.model.addObserver(this);
		this.frmIgorgui.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIgorgui = new JFrame();
		this.frmIgorgui.setTitle("IgorGui");
		frmIgorgui.setBounds(100, 100, 1020, 550);
		frmIgorgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmIgorgui
				.getContentPane()
				.setLayout(
						new MigLayout(
								"",
								"[65px:65px][250px:250px][315px:315px][200px:200px,grow]",
								"[][][30px,grow][][][30px,grow][]"));

		this.scrollPaneConsole = new JScrollPane();

		separator = new JSeparator();

		this.lblConsole = new JLabel("Console:");

		this.lblMaude = new JLabel("Maude:");

		this.lblIgor = new JLabel("IGOR:");

		this.listData = new FmodListModel();
		String defaultFmod = "<empty>";
		String[] defaultFuncs = { "invalid" };
		this.listData.addListElement(new FmodDataModel(defaultFmod, "no code",
				defaultFuncs));

		this.btnRun = new JButton("Run IGOR");

		this.progressBar = new JProgressBar(0, 100);
		this.progressBar.setFont(new Font("Tahoma", Font.BOLD, 13));
		this.progressBar.setStringPainted(true);
		this.progressBar.setValue(0);
		this.progressBar.setString("Idle");

		this.scrollPaneFmodlist = new JScrollPane();
		scrollPaneFmodlist
				.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

		this.fmodList = new JList<>(listData);
		fmodList.setVisibleRowCount(1);
		scrollPaneFmodlist.setViewportView(fmodList);
		this.fmodList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.pathMaude = new JTextField();
		this.pathMaude.setColumns(10);
		frmIgorgui.getContentPane().add(pathMaude,
				"flowx,cell 1 0,growx,aligny center");

		this.loadMaude = new JButton("Open");
		frmIgorgui.getContentPane().add(loadMaude,
				"cell 1 0,alignx trailing,aligny center");

		this.pathIgor = new JTextField();
		this.pathIgor.setColumns(10);
		frmIgorgui.getContentPane().add(pathIgor,
				"flowx,cell 1 1,growx,aligny center");

		this.loadIgor = new JButton("Open");
		frmIgorgui.getContentPane().add(loadIgor,
				"cell 1 1,alignx trailing,aligny center");

		this.loadExamples = new JButton("Load example File");
		loadExamples.setToolTipText("Load maude example file");

		frmIgorgui.getContentPane().add(loadExamples,
				"flowx,cell 2 1,alignx left");
		frmIgorgui.getContentPane().add(scrollPaneFmodlist,
				"cell 2 0,growx,aligny center");

		frmIgorgui.getContentPane().add(separator,
				"cell 0 3 4 1,growx,aligny center");
		frmIgorgui.getContentPane().add(lblConsole, "cell 0 4 2 1");

		btnKillIgor = new JButton("Kill IGOR");
		btnKillIgor.setEnabled(false);
		frmIgorgui.getContentPane().add(btnKillIgor,
				"cell 3 4,alignx right,aligny center");
		frmIgorgui.getContentPane().add(scrollPaneConsole,
				"flowx,cell 0 5 4 1,grow");

		this.consolePane = new JTextPane();
		scrollPaneConsole.setViewportView(consolePane);
		this.consolePane.setEditable(false);
		DefaultCaret consoleCaret = (DefaultCaret) this.consolePane.getCaret();
		consoleCaret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		frmIgorgui.getContentPane().add(lblMaude,
				"cell 0 0,alignx left,aligny center");

		frmIgorgui.getContentPane().add(lblIgor,
				"cell 0 1,alignx left,aligny center");

		frmIgorgui.getContentPane().add(btnRun,
				"flowx,cell 3 0,alignx left,aligny center");
		frmIgorgui.getContentPane().add(progressBar,
				"cell 3 1,alignx left,aligny center");

		this.scrollPaneFmod = new JScrollPane();
		frmIgorgui.getContentPane().add(scrollPaneFmod, "cell 0 2 4 1,grow");

		this.fmodPane = new JTextPane();
		scrollPaneFmod.setViewportView(fmodPane);
		this.fmodPane.setEditable(false);

		chckbxEnableTracing = new JCheckBox("Enable Tracing");
		frmIgorgui.getContentPane().add(chckbxEnableTracing,
				"cell 3 0,alignx right,aligny center");

		lblStatus = new JLabel("Idle");
		frmIgorgui.getContentPane().add(lblStatus, "cell 0 6 3 1");

		btnAddKnowledge = new JButton("Add Knowledge");
		frmIgorgui.getContentPane().add(btnAddKnowledge,
				"cell 2 1,alignx right");

		// this sets up the swing timer that periodically checks memory
		memChecker = new Timer(1000, new ActionListener() {

			Runtime env = Runtime.getRuntime();
			// int mb = 1024*1024;
			int mb = 1024;

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (guiWorker == null || guiWorker.isDone())
					return;

				long memTotal = env.totalMemory();
				long memFree = env.freeMemory();
				long memUsed = (memTotal - memFree);
				long percUsed = (memUsed * 100) / memTotal;

				lblStatus.setText("Mem-Total: " + (memTotal / mb)
						+ "; Mem-Free: " + (memFree / mb) + "; Mem-Used: "
						+ (memUsed / mb) + " (" + percUsed + "%)");
				if (!guiWorker.isOutputDisabled() && (percUsed >= 80)) {
					// disable console output if memory usage is 80% or greater
					guiWorker.disableOutput();
				} else {
					if ((percUsed < 80) && guiWorker.isOutputDisabled()) {
						// reenable output if memory is ok again
						guiWorker.enableOutput();
					}
				}
			}
		});

	}

	/**
	 * initialize action listeners
	 */
	private void addActionListeners() {
		loadMaude.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();

				if (lastOpenedFolder != null) {
					fc.setCurrentDirectory(lastOpenedFolder);
				}

				fc.setFileFilter(new FileFilter() {
					@Override
					public String getDescription() {
						return "Executable Files";
					}

					@Override
					public boolean accept(File f) {
						if (f.isDirectory()) {
							return true;
						} else {
							if (f.isFile()) {
								String type = "";
								try {
									// check for executable files via MIME-data
									type = Files.probeContentType(f.toPath());
									if ("application/x-msdownload".equals(type)
											|| "application/x-executable"
													.equals(type)
											|| "application/x-shellscript"
													.equals(type)) {
										return true;
									}
								} catch (Exception e) {
									e.printStackTrace();
									return false;
								}
							}
						}
						return false;
					}
				});
				fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
				int returnVal = fc.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					appendToConsole("[INFO] Maude located at "
							+ file.getAbsolutePath());
					pathMaude.setText(file.getAbsolutePath());
					lastOpenedFolder = fc.getCurrentDirectory();
				} else {
					// abort
				}
			}
		});

		loadIgor.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();

				if (lastOpenedFolder != null) {
					fc.setCurrentDirectory(lastOpenedFolder);
				}

				fc.setFileFilter(new FileNameExtensionFilter("Maude Files",
						"maude"));
				fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
				int returnVal = fc.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					appendToConsole("[INFO] IGOR located at "
							+ file.getAbsolutePath());
					pathIgor.setText(file.getAbsolutePath());
					lastOpenedFolder = fc.getCurrentDirectory();
				} else {
					// abort
				}
			}
		});

		loadExamples.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				final JFileChooser fc = new JFileChooser();

				if (lastOpenedFolder != null) {
					fc.setCurrentDirectory(lastOpenedFolder);
				}

				fc.setFileFilter(new FileNameExtensionFilter("Maude Files",
						"maude"));
				fc.removeChoosableFileFilter(fc.getAcceptAllFileFilter());
				int returnVal = fc.showOpenDialog(null);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File file = fc.getSelectedFile();
					appendToConsole("[INFO] Loading File "
							+ file.getAbsolutePath());
					lastOpenedFolder = fc.getCurrentDirectory();
					lblStatus.setText("Loading Examples");
					controller.loadExampleFile(file);
				} else {
					// abort
				}

			}
		});

		btnAddKnowledge.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				extraKnowledge = JOptionPane
						.showInputDialog(
								frmIgorgui,
								"Add extra knowledge here if IGOR requires it in the form\n 'func1 [* 'func2 * 'funcn]\n or noName if the selected module doesn't require\nextra knowledge",
								extraKnowledge);
			}
		});

		btnRun.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// before we do anything: check if the user entered valid paths
				// to maude and igor files
				if (pathMaude.getText().isEmpty()
						|| pathIgor.getText().isEmpty()) {
					showMessageToUser("You have to enter valid paths for maude and IGOR!");
					return;
				}
				// next: check if there is already a task running
				if (model.isWorking()) {
					showMessageToUser("Program is busy! Please be patient.");
					return;
				}
				// next step: check if the user selected an example for igor
				if (fmodList.getSelectedIndex() == -1) {
					showMessageToUser("You must select an example for IGOR!");
					return;
				}
				// check if selected example has more than one function and
				// let the user select which one to use
				String example = "";
				int selected = fmodList.getSelectedIndex();
				if (listData.getModuleDataAt(selected).getModuleFunctions().length > 1) {
					example = (String) JOptionPane
							.showInputDialog(
									frmIgorgui,
									"The selected module has more than 1 example.\nPlease select which example to process",
									"Example Selection",
									JOptionPane.QUESTION_MESSAGE, null,
									listData.getModuleDataAt(selected)
											.getModuleFunctions(), listData
											.getModuleDataAt(selected)
											.getModuleFunctions()[0]);
				} else {
					example = listData.getModuleDataAt(selected)
							.getModuleFunctions()[0];
				}

				appendToConsole("[MAIN] SELECTED EXAMPLE " + example);
				// last precheck - has the user selected to enable tracing? if
				// so, show her a warning message
				if (chckbxEnableTracing.isSelected()) {
					int choice = showWarningToUser("You selected to enable tracing in maude!\nThis could lead to serious resource usage and hanging,\n also the processing will take a while!\n Are you absolutely sure?");
					if (choice != JOptionPane.YES_OPTION)
						chckbxEnableTracing.setSelected(false);
				}
				String code = listData.getModuleDataAt(selected)
						.getModuleCode();
				String module = listData.getModuleDataAt(selected)
						.getModuleName();
				String maude = pathMaude.getText();
				String igor = pathIgor.getText();

				// this creates a swing worker which handles the maude task as
				// it is resource-heavy and can crash swing
				try {
					// create a PipedOutputStream and a PipedInputStream and
					// connect them together
					PipedOutputStream consoleOutput = new PipedOutputStream();
					PipedInputStream consoleInput = new PipedInputStream(
							consoleOutput);
					// create and initialize the ConsoleWorker with the created
					// pipes
					guiWorker = new IgorGuiConsoleWorker(consoleInput,
							consoleOutput, consolePane, controller);

					// setup task and start worker
					guiWorker.setupIgorTask(code, module, example,
							extraKnowledge, maude, igor);
					guiWorker.execute();
					// enable killer button
					btnKillIgor.setEnabled(true);
					// start memory watchdog
					memChecker.start();
					lblStatus.setText("Running");
				} catch (IOException e) {
					appendToConsole("[DEBUG] IOException while creating console input stream!");
					e.printStackTrace();
				}

			}
		});

		btnKillIgor.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// try to kill igor
				if (!guiWorker.isCancelled()) {
					memChecker.stop();
					guiWorker.enableOutput();
					controller.stopTask();
				}
			}
		});

		fmodList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				// sanity checks to prevent unexpected behavior when list
				// updates/changes...
				if (e.getValueIsAdjusting()
						|| fmodList.getSelectedIndex() == -1)
					return;

				int pos = fmodList.getSelectedIndex();
				appendToConsole("[INFO] Selected item "
						+ listData.getElementAt(pos));
				fmodPane.setText(listData.getModuleDataAt(pos).getModuleCode());

			}
		});
	}

	public void startProgress() {
		// let the progress bar run
		appendToConsole("[INFO] Task started");
		progressBar.setIndeterminate(true);
		progressBar.setString("Running");
	}

	public void stopProgress() {
		// stop the progress bar
		appendToConsole("[INFO] Task finished");
		progressBar.setIndeterminate(false);
		progressBar.setValue(0);
		progressBar.setString("Idle");
	}

	@Override
	public void update(Observable o, Object arg) {
		// this was used to handle messages from the tasks,
		// but is only used by the example loader. main igor task
		// handles messages different!
		String lastOutput = model.getExecMessages();
		lblStatus.setText("Idle");
		if (lastOutput != null)
			appendToConsole(lastOutput);

		// check if the model is still working or has finished its task
		if (!model.isWorking()) {
			// get task data from model
			TaskHandler lastTask = model.getLastTask();
			// check which task we have
			if (lastTask.getTaskType() == TaskTypes.TASKFMOD) {
				appendToConsole("[MAIN] Reading data from fmod parser task.");
				// update fmod list
				Object taskResult = lastTask.getResults();
				if (taskResult instanceof ArrayList<?>) {
					@SuppressWarnings("unchecked")
					ArrayList<FmodDataModel> fmodData = (ArrayList<FmodDataModel>) taskResult;

					if (fmodData.isEmpty())
						return;

					listData.clearData();
					fmodList.clearSelection();
					fmodList.setEnabled(false);
					for (FmodDataModel fmodDataModel : fmodData) {
						listData.addListElement(fmodDataModel);
					}
					fmodList.setEnabled(true);
				} else {
					appendToConsole("[MAIN] Fmod data was malformed and could not be processed!");
				}

				appendToConsole("[MAIN] Done.");
			} else if (lastTask.getTaskType() == TaskTypes.TASKIGOR) {
				// deconstruct worker - no longer needed..
				this.guiWorker = null;
				appendToConsole("[MAIN] Got result from IGOR:");
				String igorResult = (String) lastTask.getResults();
				appendToConsole(igorResult);
				btnKillIgor.setEnabled(false);
			} else {
				// something went wrong
				appendToConsole("[MAIN] Got callback from unknown task, results can not be shown.");
			}
		}
	}

	/**
	 * Displays a message dialog to the user
	 * 
	 * @param message
	 *            the message to be shown
	 */
	public void showMessageToUser(String message) {
		JOptionPane.showMessageDialog(frmIgorgui, message, "Information",
				JOptionPane.INFORMATION_MESSAGE);
	}

	/**
	 * Displays a warning dialog to the user that requires confirmation
	 * 
	 * @param message
	 *            the message to be shown
	 * @return integer value representing the user's selection
	 */
	public int showWarningToUser(String message) {
		return JOptionPane.showConfirmDialog(frmIgorgui, message, "Warning",
				JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
	}

	/**
	 * Check if tracing is selected by the user
	 * 
	 * @return boolean representing the selection state of the tracing enable
	 *         checkbox
	 */
	public boolean tracingAllowed() {
		return chckbxEnableTracing.isSelected();
	}
	
	/*
	 * shortcut-method to append text to the console text area
	 * called every time the update-method is triggered - do NOT use this mechanic for
	 * tasks with heavy workload and much text output!
	 */
	private void appendToConsole(String message) {
		Document consoleTarget = consolePane.getDocument();
		try {
			consoleTarget.insertString(consoleTarget.getLength(), ">" + message
					+ "\n", null);
		} catch (BadLocationException e) {
		}
	}
}
