package de.kawumtech.util;

/**
 * Custom data model for the example list
 * @author pete
 *
 */
public class FmodDataModel {
	
	private String fmodName;
	private String fmodCode;
	private String[] fmodFunctions;
	
	public FmodDataModel(String fmodName, String fmodCode, String[] fmodFunctions){
		this.fmodName = fmodName;
		this.fmodCode = fmodCode;
		this.fmodFunctions = fmodFunctions;
	}
	
	public String getModuleName(){
		return fmodName;
	}
	
	public String getModuleCode(){
		return fmodCode;
	}
	
	public String[] getModuleFunctions(){
		return fmodFunctions;
	}
}
