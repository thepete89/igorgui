package de.kawumtech.main;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import de.kawumtech.logic.IgorController;
import de.kawumtech.logic.IgorModel;

public class IgorGui {
	
	// main entry point for gui
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(
		            UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Fehler",
					JOptionPane.ERROR_MESSAGE);
		}
				
		SwingUtilities.invokeLater(new Runnable() {	
			@Override
			public void run() {
				IgorModel model = new IgorModel();
				@SuppressWarnings("unused")
				IgorController controller = new IgorController(model);				
			}
		});
	}

}
