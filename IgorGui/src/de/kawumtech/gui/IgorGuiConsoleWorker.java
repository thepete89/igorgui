package de.kawumtech.gui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.swing.JTextPane;
import javax.swing.SwingWorker;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import de.kawumtech.logic.IgorController;
import de.kawumtech.task.IgorTaskHandler;

/**
 * This ConsoleWorker which extends the {@link SwingWorker} class handles the output comming from the {@link IgorTaskHandler} and
 * passes it on to the main window's console text area
 * @author pete
 *
 */
public class IgorGuiConsoleWorker extends SwingWorker<Integer, String> {
	// maximum text buffer size of console text area
	private final int CHARBUFFER_SIZE = 10000;
	
	// worker inputs/outputs
	private PipedInputStream consolePipe;
	private PipedOutputStream subprocessPipe;
	
	// document element of the target text area
	private Document consoleTarget;
	
	// igor controller
	private IgorController controller;
	
	// igor task data
	private String fmodFile;
	private String fmodModule;
	private String fmodExample;
	private String extraKnowledge;
	private String maudePath;
	private String igorPath;
	
	// output control
	private volatile boolean disableOutput;
	
	public IgorGuiConsoleWorker(PipedInputStream consolePipe, PipedOutputStream subprocessPipe, JTextPane consoleTarget, IgorController controller){
		this.consolePipe = consolePipe;
		this.subprocessPipe = subprocessPipe;
		this.consoleTarget = consoleTarget.getDocument();
		this.controller = controller;
	}
	
	public void setupIgorTask(String fmodFile, String fmodModule, String fmodExample, String extraKnowledge, String maudePath, String igorPath){
		this.fmodFile = fmodFile;
		this.fmodModule = fmodModule;
		this.fmodExample = fmodExample;
		this.extraKnowledge = extraKnowledge;
		this.maudePath = maudePath;
		this.igorPath = igorPath;
	}
	
	public synchronized void disableOutput(){
		this.disableOutput = true;
		publish("[CONSOLE] WARNING: Application memory overloaded - temporarily disabled output!");
	}
	
	public synchronized void enableOutput(){
		if(this.disableOutput){
			publish("[CONSOLE] Memory back to normal - output reenabled.");
			this.disableOutput = false;
		}
	}
	
	public synchronized boolean isOutputDisabled(){
		return this.disableOutput;
	}
	
	@Override
	protected Integer doInBackground() throws Exception {
		publish("[CONSOLE] Console Worker started!");
		// setting up input from task
		BufferedReader reader = new BufferedReader(new InputStreamReader(consolePipe));
		String line = null;
		disableOutput = false;

		// start task
		controller.runIgor(fmodFile, fmodModule, fmodExample, extraKnowledge, maudePath, igorPath, subprocessPipe);
		while((line = reader.readLine()) != null){
			if(!disableOutput){
				publish(line);
			}
			
			if(isCancelled()){
				publish("[CONSOLE] Worker cancelled - stopping...");
				controller.stopTask();
				reader.close();
				return 1;
			}
			// very short break here to let the console catch up
			Thread.sleep(0, 50);
		}
		publish("[CONSOLE] Console input stream closed.");
		reader.close();
		publish("[CONSOLE] Console worker finished - closing and triggering update.");
		return 0;
	}	
	
	@Override
	protected void process(List<String> chunks) {
		// cleanup console from time to time
		if(consoleTarget.getLength() > CHARBUFFER_SIZE){
			try {
				consoleTarget.remove(0, consoleTarget.getLength());
			} catch (BadLocationException e) {
			}
		}
		Iterator<String> it = chunks.iterator();
		while(it.hasNext()){
			try {
				consoleTarget.insertString(consoleTarget.getLength(), "> " + it.next() + "\n", null);

			} catch (BadLocationException e) {
			}
		}
	}

	@Override
	protected void done() {
		// close I/O-Pipes
		try {
			this.consolePipe.close();
			this.subprocessPipe.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// trigger update
			controller.triggerManualUpdate();
		}
	}





}
