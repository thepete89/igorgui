package de.kawumtech.logic;

import java.io.File;
import java.io.PipedOutputStream;
import java.util.Observable;
import java.util.Observer;

import de.kawumtech.gui.IgorGuiMainwindow;

/**
 * central controller for igor gui, controlling all available actions
 * @author pete
 *
 */
public class IgorController implements Observer {

	private IgorModel model;
	private IgorGuiMainwindow window;

	public IgorController(IgorModel model) {
		this.model = model;
		this.model.addObserver(this);
		this.window = new IgorGuiMainwindow(this, model);
	}

	public void loadExampleFile(File file) {
		if (model.isWorking()) {
			window.showMessageToUser("Program is busy! Please be patient.");
		} else {
			window.startProgress();
			model.loadExampleFile(file);
		}
	}

	public void runIgor(String fmodFile, String fmodModule, String fmodExample, String extraKnowledge,
			String maudePath, String igorPath, PipedOutputStream console) {

		window.startProgress();
		model.runIgor(fmodFile, fmodModule, fmodExample, extraKnowledge, maudePath, igorPath,
				console, window.tracingAllowed());
	}
	
	// workaround for the console worker after the igor task has finished
	public void triggerManualUpdate() {
		model.signalizeUpdate();
	}

	public boolean hasWork() {
		return model.isWorking();
	}

	public void stopTask() {
		model.stopCurrentTask();
	}

	@Override
	public void update(Observable o, Object arg) {
		// check if model has finished work
		if (!model.isWorking()) {
			// stop the progress bar from spinning
			window.stopProgress();
		}
	}

}
