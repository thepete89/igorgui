package de.kawumtech.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.kawumtech.task.TaskHandler;

/**
 * Handler thread which manages maude's InputStream (from Java's perspective - for maude this is the StandardOutput)
 * @author pete
 *
 */
public class InputStreamHandler extends Thread {
	
	private InputStream progInStream;
	private TaskHandler taskHandler;
	private String handlerName;
	private boolean isProcessing = false;
	private volatile boolean stopInput = false;
	
	public InputStreamHandler(InputStream progInStream, TaskHandler taskHandler, String handlerName){
		this.progInStream = progInStream;
		this.taskHandler = taskHandler;
		this.handlerName = handlerName;
		this.setName(handlerName);
	}

	@Override
	public void run() {
		BufferedReader br = new BufferedReader(new InputStreamReader(progInStream));
		String line = null;
		this.isProcessing = true;
		try {
			while((line = br.readLine()) != null && !stopInput){
				taskHandler.pushTaskOutput(line);
			}
			taskHandler.pushTaskOutput("[" + handlerName + "] Program input stream closed.");
		} catch (IOException e) {
			taskHandler.pushTaskOutput("[" + handlerName +"] IOEXCEPTION WHILE READING FROM PROCESS INPUT - ABORTING.");
		} finally {
			this.isProcessing = false;
			try {
				br.close();
			} catch (IOException e) {
				// no need to catch this
			}
		}
	}
	
	public boolean isProcessing(){
		return isProcessing;
	}
	
	public synchronized void stopProcessing(){
		this.stopInput = true;
	}
	
}
