package de.kawumtech.task;

import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import de.kawumtech.igorparser.IgorParser;
import de.kawumtech.util.ExtManager;
import de.kawumtech.util.InputStreamHandler;

/**
 * {@link TaskHandler} implemented to handle the igor/maude task
 * which executes examples and parses the generated output
 * @author pete
 *
 */
public class IgorTaskHandler implements TaskHandler {

	// task active flag
	private boolean isActive = false;
	// task error flag
	private boolean error = false;
	// work parameters
	private String fmodFile;
	private String fmodModule;
	private String fmodExample;
	private String knowledge;
	private String maudePath;
	private String igorPath;
	private boolean traceEnabled = false;
	// task data
	private List<String> extOutput;
	private String igorResult;
	// task output stream - for swing load balancing
	private PrintWriter taskPrintWriter;
	// process handler
	private ExtManager process;
	// streams
	private PrintWriter command;
	private InputStreamHandler ish;
	// original sys err
	private PrintStream sysErr;

	public IgorTaskHandler(String fmodFile, String fmodModule,
			String fmodExample, String knowledge, String maudePath, String igorPath,
			PipedOutputStream swingConsole, boolean trace) {
		this.fmodFile = fmodFile;
		this.fmodModule = fmodModule;
		this.fmodExample = fmodExample;
		this.knowledge = knowledge;
		this.maudePath = maudePath;
		this.igorPath = igorPath;
		this.extOutput = Collections.synchronizedList(new ArrayList<String>());
		this.taskPrintWriter = new PrintWriter(swingConsole, true);
		this.traceEnabled = trace;
		this.process = new ExtManager(maudePath, "");
		// redirect error stream.. for igor-parser.. narf
		this.sysErr = System.err;
		System.setErr(new PrintStream(swingConsole));
		
	}

	@Override
	public void run() {
		startTask();
	}

	@Override
	public void startTask() {
		this.isActive = true;
		pushTaskOutput("[IGOR-TASK] Starting maude at " + this.maudePath
				+ " calling IGOR at " + this.igorPath);

		runIgor();
		// only process output if igor wasn't killed
		if(!error){
			processIgorOutput();
		}
		// we are done here
		this.isActive = false;
		pushTaskOutput("[IGOR-TASK] Task finished!");
		taskPrintWriter.close();
		System.setErr(sysErr);
	}

	private void runIgor() {
		try {
			// start maude
			process.execute();

			ish = new InputStreamHandler(
					process.getProcessOutput(), this, "MAUDE-OUTPUT");
			
			command = new PrintWriter(process.getProcessInput(), true);
			ish.start();

			// debug - enable tracing
			if(traceEnabled)
				command.println("set trace on .");
			// send commands
			command.println("load " + this.igorPath);
			command.println(this.fmodFile);
			command.println("red in IGOR : gen('" + this.fmodModule + ", '"
					+ this.fmodExample + ", " + this.knowledge + ") .");
			command.println("eof");
			
			// wait for maude to finish/check exit status
			int status = process.waitFor();			
			// wait for input stream handler to finish
			while(ish.isProcessing()){
				// do nothing
				Thread.sleep(1);
			}
			
			// close streams
			if(ish.isProcessing())
				ish.stopProcessing();
			command.close();
						
			pushTaskOutput("[IGOR-TASK] Maude finished with exit code "
					+ status);
			
			if(status > 0){
				pushTaskOutput("[IGOR-TASK] Maude exit code was greater 0 - output-processing disabled.");
				this.error = true;
				this.igorResult = "ERROR";
			}
			
		} catch (RuntimeException | InterruptedException e) {
			pushTaskOutput("[IGOR-TASK] ERROR: Encountered exception "
					+ e.getMessage());
			// e.printStackTrace();
			// System.out.println("[IGOR-TASK] Abnormal termination of task.");
			this.error = true;
			this.igorResult = "ERROR";
		}
	}

	private void processIgorOutput() {
		boolean foundResult = false;
		StringBuilder sb = new StringBuilder();
		synchronized (extOutput) {
			Iterator<String> it = extOutput.iterator();
			while (it.hasNext()) {
				String line = it.next();
				if (line.startsWith("result")) {
					foundResult = true;
					// replace preambles of first line or parser has problems...
					line = line.replaceFirst("result Hypo: ", "");
					line = line.replaceFirst("result HypoList: ", "");
				}
				if ("Bye.".equals(line)) {
					foundResult = false;
				}
				if (foundResult) {
					// cleanup
					line = line.trim();
					line = line.replaceFirst("nextHypo", " ");
					// processing
					sb.append(line);
				}
			}
		}
		String res = sb.toString();
		pushTaskOutput("[IGOR-TASK] PROCESSED: ");
		pushTaskOutput(res);
		try {
			this.igorResult = new IgorParser().parseInput(res);
		} catch (Exception e) {
			pushTaskOutput("[IGOR-TASK] ERROR: Encountered parser exception: ");
			e.printStackTrace();
			this.error = true;
			this.igorResult = "ERROR";
		}
	}

	@Override
	public boolean isTaskActive() {
		return isActive;
	}

	@Override
	public Object getResults() {
		return igorResult;
	}

	@Override
	public void pushTaskOutput(String output) {
		// manager-method for handling maude output
		// write to console manager stream
		taskPrintWriter.println(output);
		// write to internal buffer if output is no informatiom from handlers
		if (!output.startsWith("[")) {
			extOutput.add(output);
		}

	}

	@Override
	public TaskTypes getTaskType() {
		return TaskTypes.TASKIGOR;
	}

	@Override
	public synchronized void cancelTask() {
		process.close();
	}

}
