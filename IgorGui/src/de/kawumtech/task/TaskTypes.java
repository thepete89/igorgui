package de.kawumtech.task;

/**
 * Enum containing different valid task types used by the {@link TaskHandler}s
 * @author pete
 *
 */
public enum TaskTypes {
	TASKIGOR, TASKFMOD
}
